#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Saurav Tuladhar'
SITENAME = u'Saurav R Tuladhar'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'
THEME = './theme/pelican-octopress-theme'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5
STATIC_PATHS = ['images']
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Title menu options
MENUITEMS = [('Blog','/'),('About', 'pages/about.html')]
PLUGIN_PATHS = ['plugins']
DISPLAY_PAGES_ON_MENU = False

# Pelican plugins:
PLUGINS = ['render_math']
