Title: About 
Date: 2017-03-03 09:20
Slug: about
Authors: Saurav Tuladhar 
Summary: About me. 

I am a researcher and an engineer with background in the field of signal processing.
I completed my Ph.D. in Electrical Engineering from 
the [University of Massachusetts Dartmouth](http://www.umassd.edu) working with
[John R. Buck](http://www.umassd.edu/engineering/ece/people/facultyandstaff/johnrbuck/).
As a part of my doctoral research I developed adaptive beamforming algorithms 
to suppress loud interferers in a training data starved scenarios. Specifically
I designed a new technique to improve the minimum variance
distortionless response (MVDR) beamformer algorithm. 

My current work focuses on:   

- Optimal filtering for aircraft/UAV tracking.
- Bayesian inference for machine learning
- Computer vision techniques for visual object tracking.

The posts in this blog are composed using the Jupyter notebook.The blog itself
is created using Pelican, the static site generator.
