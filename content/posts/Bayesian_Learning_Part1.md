Title: Bayesian Learning: Part I
Slug: bayesian-learning-1
Author: Saurav Tuladhar
Date: 04-04-2017
Tags: bayesian, machine learning
Category: post

In this post I want to lay out some of the basic concepts of Bayesian inference as used in the machine learning context. The content on this post is heavily derived from [1, 2]. This post and my interes in Bayesian inference has been inspired by talks I attend with the [Boston Bayesian Meetup](https://www.meetup.com/Boston-Bayesians) group.

In supervised machine learning, we wish to learn relation between two variables $A$ and $B$ given some sample examples. Once the relation has been 'learned', the we can predict 'what is $B$' given $A$. In practice the measurement of both the dependent ($B$) and independent ($A$) variable is inexact and the relation between them is not deterministic. Given this inherent uncertainty it does not make sense to have a deterministic for $B$ with complete certainty. A probabilistic approach is better suited for meaningful reasoning in presence of uncertainty.

The probabilistic approach treats both $A$ and $B$ as random variables and relates the two via the conditional probability $P(B|A)$ i.e., the probability of $B$ given a specific value of $A$. The first step in machine learning is to approximate the likelihood $P(B|A)$ with an appropriate model based on a given set of examples of $A$ and $B$. Typically, the conditional probability $P(B|A)$ has a parameterized model 
$$
\newcommand{\weight}{\mathbf{w}}
\newcommand{\dataset}{\mathcal{D}}
\newcommand{\paramvec}{\mathbf{\theta}}
\newcommand{\xvec}{\mathbf{x}}
\newcommand{\xmat}{\mathbf{X}}
\newcommand{\yvec}{\mathbf{y}}
\newcommand{\realnum}{\mathbb{R}}
\newcommand{eye}{\mathbf{I}}
\newcommand{\cov}{\mathbf{V}}
\newcommand{\inv}{{-1}}
P(B|A) = f(A;\weight),
$$
where $\weight$ is a vector of model parameters. Then given a set $\dataset$ of $N$ examples $\dataset = \{A_n, B_n\}_{n=1:N}$, a conventional approach involves optimization (e.g. maximum likelihood, least-squares fit) of our model for the given data $\dataset$ w.r.t. the parameters $\weight$. With the optimal parameters $\weight_{opt}$ computed, we can predict the unknown $B$ by evaluating $f(A_*; \weight_{opt})$ given $A_*$. 

The Bayesian inference approach treats the model parameters $\weight$ as random variables. The conditional probability of $B$ now depends on both $A$ and $\weight$: $P(B|A, \weight)$. Rather than 'learning' specific values for $\weight$, we can infer a *posterior* distribution $P(\weight|A, B)$ over the parameters $\weight$ using the Bayes' rule. 

In general, the Bayesian approach for learning model parameters involves following steps:
* Given $n$ data, $\dataset$, write down the likelihood expression $P(B|, \weight)$.
* Specify a prior: $p(\weight)$
* Compute the posterior
$$ p(\weight|B) = \frac{p(B|\weight)p(\weight)}{p(B)}.$$


## Linear Prediction
Lets look at an example of the classic linear regression problem. A training dataset $\dataset$ with $N$ examples $\{\xvec_{1:N}, y_{1:N}\}$ is given. Each input $\xvec_i \in \realnum^M$ is a vector with $M$ attributes and each output $y_i \in \realnum$ is considered univariate. In linear prediction, the relation between input and output in the training dataset is approximated by a parameterized linear model $y(\xvec, \paramvec)$ where parameter vector $\paramvec = \{\theta_1, \theta_2,\ldots,\theta_M\}$. The linear model is expressed as:
$$
y_i = \sum_{j=1}^M x_{ij}\theta_j, 
$$
or a more compact matrix form is
$$
\yvec = \xmat\paramvec,
$$
where $\yvec$ is an $N\times1$ vector whose entries are $N$ outputs of training data and $\xmat$ is an $N\times M$ matrix whose rows are the input vectors of the training dataset. The problem is to estimate the parameters $\paramvec$ such that our linear model gives the 'best' fit to the provided training dataset $\dataset$. The estimated parameter $\hat{\theta}$ can be used to predict the ouput for input values not in the training dataset.

### Least squares
The least squares (LS) estimate of the parameter.
$$
\hat{\paramvec} = (\xmat^T\xmat)^{-1}\xmat^T\yvec
$$

### Maximum likelihood 
The maximum likelihood (ML) estimate of the parameters $\paramvec$ is same as the LS estimate. Hence the LS parameter estimate is optimal in ML sense.

### Bayesian linear regression
*Posterior mean*: $\paramvec = (\lambda\eye_M + \xmat^T\xmat)^{\inv}\xmat^T\yvec$   
*Posterior variance*: $\cov_N = \sigma^2(\lambda\eye_M + \xvec^T\xvec)^{\inv}$
where $\lambda$ is a constant (to be defined).

## References
1. Bayesian Inference: An Introduction to Principles and Practice in Machine Learning, Michael E. Tipping, Microsoft Research, Cambridge, U.K. (http://www.miketipping.com/papers.htm)   
2. Machine Learning (CPSC 540) lecture notes from Prof. Nando de Freitas, UBC (http://www.cs.ubc.ca/~nando/540-2013/index.html)
